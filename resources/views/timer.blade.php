@extends('layouts.layout')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/timer.css') }}">
@endsection

@section('title')
    123
@endsection

@section('body')
    <section class="timer">
        <div class="timer-rectangle">
            <div id="countdown">
            <div id="tiles"></div>
            </div>
        <div class="label">
            <h1>Days</h1>
            <h1>Hours</h1>
            <h1>Minutes</h1>
            <h1>Seconds</h1>
        </div>
        </div>
    </section>

    <script>
        var target_date = new Date(2021, 0, 1, 0, 0, 0, 0); // установить дату обратного отсчета
        var days, hours, minutes, seconds; // переменные для единиц времени

        var countdown = document.getElementById("tiles"); // получить элемент тега

        getCountdown();

        setInterval(function () { getCountdown(); }, 1000);

        function getCountdown(){

            var current_date = new Date().getTime();
            var seconds_left = (target_date - current_date) / 1000;

            days = pad( parseInt(seconds_left / 86400) );
            seconds_left = seconds_left % 86400;

            hours = pad( parseInt(seconds_left / 3600) );
            seconds_left = seconds_left % 3600;

            minutes = pad( parseInt(seconds_left / 60) );
            seconds = pad( parseInt( seconds_left % 60 ) );

            // строка обратного отсчета  + значение тега
            countdown.innerHTML = "<span>" + days + "</span><span>" + hours + "</span><span>" + minutes + "</span><span>" + seconds + "</span>";
        }

        function pad(n) {
            return (n < 10 ? '0' : '') + n;
        }

    </script>
@endsection
