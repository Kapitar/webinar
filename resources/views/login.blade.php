@extends('layouts.layout')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/login.css') }}">
@endsection

@section('title')
    123
@endsection

@section('body')
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <section class="login">
        <div class="form-login">
            <div class="form-content">
                <h2>Please Enter Your Full Name and Email ID to Enter the Event:</h2>

                <form action="{{ route('login') }}" method="post">

                    @csrf
                    <input class="form-input first" name="name" type="text" placeholder="Full Name">
                    <input class="form-input second" name="email" type="text" placeholder="Email ID">
                    <button type="submit" class="form-btn login">
                        Login Now
                    </button>
                </form>
            </div>
        </div>
    </section>

    <script>
        $('button.login').click(function() {
            console.log(312)
            var url = 'https://cubingbattle.ru/api/v1/widgets/clicker/Login';
            $.get(url, function(response) {
                console.log(response);
            });
        });
    </script>
@endsection
