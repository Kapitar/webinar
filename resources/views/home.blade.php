@extends('layouts.layout')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/home.css') }}">
@endsection

@section('title')
    123
@endsection

@section('body')
    <section class="home">
        <div class="video">
            <video controls="" style="display: block; object-fit: cover; background-size: cover; opacity: 1; width: 363px; height: 179px; letter-spacing: 0px; font-weight: 400; font-size: 12px; border-color: rgb(255, 255, 255); border-style: none; margin: 0px; border-radius: 0px; padding: 0px;" loop="" preload="auto" data-stylerecorder="true"><source src="{{ asset('assets/images/video.mp4') }}" type="video/mp4" data-stylerecorder="true" style=" display: block;line-height: 18px; letter-spacing: 0px; font-weight: 400; border-color: rgb(0, 0, 0); border-style: none; margin: 0px; border-radius: 0px; padding: 0px;"></video>
        </div>
        <div class="button-group">
            <a href="{{route('attendClicker')}}">
            <button class="btn-org attend">
                Attend Webinar
            </button>
            </a>
            <br>
            <a href="{{route('agendaClicker')}}">
                <button class="btn-org agenda">
                    Agenda
                </button>
            </a>
                <br>
            <a href="{{route('brochuresClicker')}}">
            <button class="btn-org brochures">
                Brochures
            </button>
            </a>
        </div>
        <button class="btn-org btn-help">
            May I Help You?
        </button>
    </section>

    <script>
        $('button.attend').click(function() {
            console.log(312)
            var url = 'https://cubingbattle.ru/api/v1/widgets/clicker/Attend_Webinar';
            $.get(url, function(response) {
                console.log(response);
            });
        });

        $('button.agenda').click(function() {
            console.log(123)
            var url = 'https://cubingbattle.ru/api/v1/widgets/clicker/Agenda';
            $.get(url, function(response) {
                console.log(response);
            });
        });

        $('button.brochures').click(function() {
            console.log(12344)
            var url = 'https://cubingbattle.ru/api/v1/widgets/clicker/Brochures';
            $.get(url, function(response) {
                console.log(response);
            });
        });
    </script>
@endsection
