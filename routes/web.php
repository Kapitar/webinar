<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/',
    [\App\Http\Controllers\HomeController::class, 'index']
);

Route::post(
    'login/submit',
    'HomeController@login'
)->name('login');


Route::get(
    'agenda',
    [\App\Http\Controllers\HomeController::class, 'agenda']
)->name('agenda');

Route::get(
    'timer',
    [\App\Http\Controllers\HomeController::class, 'timer']
)->name('timer');

Route::get(
    'analytics',
    [\App\Http\Controllers\HomeController::class, 'analytics']
)->name('analytics');

Route::get(
    'agenda/clicker',
    [\App\Http\Controllers\HomeController::class, 'agendaClick']
)->name('agendaClicker');

Route::get(
    'attend_webinar/clicker',
    [\App\Http\Controllers\HomeController::class, 'attendClick']
)->name('attendClicker');

Route::get(
    'brochures/clicker',
    [\App\Http\Controllers\HomeController::class, 'brochuresClick']
)->name('brochuresClicker');

