<?php 

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class TestService extends Facade {
	
	protected static function getFacadeAccessor() {
		return 'TestService';
	}

}