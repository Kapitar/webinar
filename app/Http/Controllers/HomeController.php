<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

class HomeController extends Controller
{

    public function analytics() {
        return view('analytic');
    }

	public function agenda() {
    	return view('agenda');
    }

    public function timer() {
    	return view('timer');
    }

	public function login(UserRequest $req) {

		$user = DB::table('users')->select('name')->where('email', $req->input('email'))->first();

		if($user == null) {
            $client  = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote  = @$_SERVER['REMOTE_ADDR'];
            $result  = array('country'=>'', 'city'=>'');
             
            if(filter_var($client, FILTER_VALIDATE_IP)) $ip = $client;
            elseif(filter_var($forward, FILTER_VALIDATE_IP)) $ip = $forward;
            else $ip = $remote;
             
            $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));    
            if($ip_data && $ip_data->geoplugin_countryName != null)
            {
                $country = $ip_data->geoplugin_countryCode;
            }

			DB::table('users')->insert(
			  ['name' => $req->input('name'), 'email' => $req->input('email'), 'country' => $country]
			);
		}

		$req->session()->put('email', $req->input('email'));

		return redirect('/');
	}

    public function index(Request $req) {
    	if ($req->session()->has('email')) {
		  return view('home');
		}
        return view('login');
    }

    public function getUsers() {
        return response()->json(DB::table('users')->get());
    }

    public function getClicks() {
        return response()->json(DB::table('clicks')->get());
    }

    public function setClicker($button) {
        $clicks = DB::table('clicks')->where('button', $button)->value('count');
        DB::table('clicks')->where('button', $button)->update(
            ['count' => $clicks+1]
        );

        return response()->json('success');
    }

    public function agendaClick(Request $req) {
        $email = $req->session()->get('email');
        $clicks = DB::table('users')->where('email', $email)->value('agenda');
        DB::table('users')->where('email', $email)->update(
            ['agenda' => $clicks+1]
        );

        return redirect()->route('agenda');
    }

    public function attendClick(Request $req) {
        $email = $req->session()->get('email');
        $clicks = DB::table('users')->where('email', $email)->value('attend_webinar');
        DB::table('users')->where('email', $email)->update(
            ['attend_webinar' => $clicks+1]
        );

        return redirect('/');
    }

    public function brochuresClick(Request $req) {
        $email = $req->session()->get('email');
        $clicks = DB::table('users')->where('email', $email)->value('brochures');
        DB::table('users')->where('email', $email)->update(
            ['brochures' => $clicks+1]
        );

        return redirect('/');
    }
}
